require'nvim-treesitter.configs'.setup {
    ensure_installed = {"go"},
    -- List of parsers to ignore installing (or "all")
    -- ignore_install = { "jsonc" },

    sync_install = false,
    auto_install = true,
    highlight = {enable = true}
}
