vim.g.mapleader = ","

-- NeoTree
vim.keymap.set('n', '<c-e>', ':Neotree left reveal<CR>')

-- Navigation
vim.keymap.set('n', '<c-k>', ":call WinMove('k')<CR>", {silent = true})
vim.keymap.set('n', '<c-j>', ":call WinMove('j')<CR>", {silent = true})
vim.keymap.set('n', '<c-h>', ":call WinMove('h')<CR>", {silent = true})
vim.keymap.set('n', '<c-l>', ":call WinMove('l')<CR>", {silent = true})

-- LSP
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
vim.keymap.set('n', '<leader>lD', vim.diagnostic.open_float)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next)
vim.keymap.set('n', '<leader>ld', vim.diagnostic.setloclist)

-- move lines in normal mode. 'm' - move up, 'n' -move down
vim.api.nvim_set_keymap('n', 'n', ':m .+1<CR>==',{noremap = true, silent = true})
vim.api.nvim_set_keymap('n', 'm', ':m .-2<CR>==',{noremap = true, silent = true})

-- move lines in visual mode. 'm' - move up, 'n' -move down
vim.api.nvim_set_keymap('v', 'n', ':m \'>+1<CR>gv=gv',{noremap = true, silent = true})
vim.api.nvim_set_keymap('v', 'm', ':m \'<-2<CR>gv=gv',{noremap = true, silent = true})

-- Formatter
vim.cmd([[
  augroup FormatAutogroup
    autocmd!
    autocmd User FormatterPre lua print "This will print before formatting"
    autocmd User FormatterPost lua print "This will print after formatting"
  augroup END
]])

vim.cmd([[
  autocmd FileType go nmap <leader>r  <Plug>(go-run)
  autocmd FileType go nmap <leader>t  <Plug>(go-test)
  autocmd FileType go nmap <Leader>c <Plug>(go-coverage-toggle)
  autocmd FileType go nmap <Leader>i <Plug>(go-info)
]])

-- WinMove
vim.cmd([[
  function! WinMove(key)
	  let t:curwin = winnr()
	  exec "wincmd ".a:key
	  if (t:curwin == winnr())
		  if (match(a:key, '[jk]'))
			  wincmd v
		  else
			  wincmd s
		  endif
		  exec "wincmd ".a:key
	  endif
  endfunction
]])
