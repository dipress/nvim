### Dependencies

For MacOS:

```bash
    brew install xclip
    brew install ripgrep
```

For Linux (Ubuntu):

```bash
    apt install xclip
    apt install ripgrep
```
