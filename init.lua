-- Basic configuration
require('core.plugins')
require('core.mappings')
require('core.colors')
require('core.configs')

-- Plugins
require('plugins.neotree')
require('plugins.treesitter')
require('plugins.lsp')
require('plugins.lualine')
require('plugins.cmp')
require('plugins.mason')
require('plugins.nulls')
require('plugins.telescope')
